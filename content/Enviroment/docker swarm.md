---
title: "Docker Swarm"
date: 2019-08-05T10:21:50-05:00
draft: false
tags: ["docker", "swarm"]
menuTitle : "Docker swarm"
---
![Swarm?](/img/docker.svg?height=300px)


### Inicializa el cluster
___

    docker swarm init

#### Fix 

    sudo chmod 777 /var/run/docker.sock

#### Docker Desktop with WSL2

    https://docs.docker.com/docker-for-windows/wsl-tech-preview/