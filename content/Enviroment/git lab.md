---
title: "SSH keys en Gitlab"
date: 2019-08-05T10:21:50-05:00
draft: false
tags: ["gitlab", "ssh", "keys"]
menuTitle : "SSH keys"
---

___
### Crear SSH keys

1. Generar un nuevo par de llaves SSH
    *  ED25519 SSH key

            ssh-keygen -t ed25519 -C "email@example.com"

     *  RSA SSH key

            ssh-keygen -t rsa -b 4096 -C "email@example.com"

Next, you will be prompted to input a file path to save your SSH key pair to. If you don’t already have an SSH key pair and aren’t generating a deploy key, use the suggested path by pressing ```Enter```. Using the suggested path will normally allow your SSH client to automatically use the SSH key pair with no additional configuration.

If you already have an SSH key pair with the suggested file path, you will need to input a new file path and declare what host this SSH key pair will be used for in your ~/.ssh/config file.

Once the path is decided, you will be prompted to input a password to secure your new SSH key pair. It’s a best practice to use a password, but it’s not required and you can skip creating it by pressing `Enter` twice.

If, in any case, you want to add or change the password of your SSH key pair, you can use the -p flag:


#### Agregar SSH keys a cuenta de GitLab

1. Copia tu llave SSH pública

    Linux

        xclip -sel clip < ~/.ssh/id_ed25519.pub

    Windows

        cat ~/.ssh/id_ed25519.pub | clip
            
2. En tu cuenta de Gitlab pega la llave, ve a: Settings -> Repository -> Deploy Keys -> Expand

{{% notice tip %}}

Asegurate que git config --global user.name "YOUR_USERNAME" y user.email sea como indica la cuenta de gitlab

{{% /notice %}}



