---
title: "mxShop Wordpress"
date: 2019-08-05T10:21:50-05:00
draft: false
tags: ["docker", "swarm", "wordpress"]
menuTitle : "mxShop"
---
![Swarm?](/img/wordpress-blue.svg?height=300px)


___
### Crea vm en cloud

*  Requiere que sea n1 primera generación, usé ubuntu 20.04 lts minimal


___
### Instala cluster

*  apt-get update && upgrade

*  Instala docker con snap

*  Inicializa el cluster, docker swarm

*  Instala portainer [Documentación](https://www.portainer.io/installation/)

        docker volume create portainer_data
        docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

___
### Wordpress docker-compose

*   Crea el upload.ini y el docker-compose.yaml

*   Inserta el contenido y

            docker-compose up -d

*   Instala wordpress

#### Instala el tema y plugins

*   Sube el tema, activalo, activa el plugin xstore core

*   Import demo data electronics

*   Sube e instala los plugins: visual-composer, woocommerce-infinite-scroll, smartproductviewer, massive-addons, y los otros 7.

*   Importar datos