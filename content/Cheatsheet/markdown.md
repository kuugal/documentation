---
title: "Markdown"
tags: ["markdown", "cheatsheet"]
menuTitle : "Markdown"
---

![Markdown](/img/markdown-logo.png?height=300px)


### Comandos
___

# Heading 1 #
```markdown
# Heading 1 #

-O-

Heading 1
============= (debajo del H1)
```
## Heading 2 ##
```markdown
## Heading 2 ##

-O-

Heading 2
--------------- (below H2 text)
```
### Heading 3 ###
```markdown
### Heading 3 ###
```
#### Heading 4 ####
```markdown
#### Heading 4 ####
```
Common text
```markdown
Common text
```
_Emphasized text_
```markdown
_Emphasized text_ or *Emphasized text*
```
~~Strikethrough text~~
```markdown
~~Strikethrough text~~
```
__Strong text__
```markdown
__Strong text__ or **Strong text**
```
___Strong emphasized text___
```markdown
___Strong emphasized text___ or ***Strong emphasized text***
```

[Named Link](http://www.google.fr/ "Named link title") and http://www.google.fr/ or <http://example.com/>
```markdown
[Named Link](http://www.google.fr/ "Named link title") and http://www.google.fr/ or <http://example.com/>
```
[heading-1](#heading-1 "Goto heading-1")
```markdown    
[heading-1](#heading-1 "Goto heading-1")
```
Table, like this one :

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

```markdown
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

`code()`
```markdown
`code()`
```
```javascript
    var specificLanguage_code = 
    {
        "data": {
            "lookedUpPlatform": 1,
            "query": "Kasabian+Test+Transmission",
            "lookedUpItem": {
                "name": "Test Transmission",
                "artist": "Kasabian",
                "album": "Kasabian",
                "picture": null,
                "link": "http://open.spotify.com/track/5jhJur5n4fasblLSCOcrTp"
            }
        }
    }
```

    ```javascript
    var spe....
    ```

    ``` or ~~~

* Bullet list
    * Nested bullet
        * Sub-nested bullet etc
* Bullet list item 2

```markdown
* Bullet list
    * Nested bullet
        * Sub-nested bullet etc
* Bullet list item 2
```

1. A numbered list
    1. A nested numbered list
    2. Which is numbered
2. Which is numbered

~~~markdown
1. A numbered list
    1. A nested numbered list
    2. Which is numbered
2. Which is numbered
~~~

- [ ] An uncompleted task
- [x] A completed task

~~~markdown
- [ ] An uncompleted task
- [x] A completed task
~~~

> Blockquote
>> Nested blockquote
```markdown
> Blockquote
>> Nested Blockquote
```
_Horizontal line :_
- - - -
```markdown
- - - -
```
_Image with alt :_

![picture alt](http://via.placeholder.com/200x150 "Title is optional")
```markdown
![picture alt](http://via.placeholder.com/200x150 "Title is optional")
```

Hotkey:

<kbd>⌘F</kbd>

<kbd>⇧⌘F</kbd>

    <kbd>⌘F</kbd>

Hotkey list:

| Key | Symbol |
| --- | --- |
| Option | ⌥ |
| Control | ⌃ |
| Command | ⌘ |
| Shift | ⇧ |
| Caps Lock | ⇪ |
| Tab | ⇥ |
| Esc | ⎋ |
| Power | ⌽ |
| Return | ↩ |
| Delete | ⌫ |
| Up | ↑ |
| Down | ↓ |
| Left | ← |
| Right | → |