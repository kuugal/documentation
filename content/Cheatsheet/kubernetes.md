---
title: "Kubernetes"
tags: ["Kubernetes", "Cheatsheet"]
menuTitle : "Kubernetes"
---
![Docker](/img/kubernets.svg?height=360px)
### Comandos
___

| Comando | Descripción |
| ------ | ----------- |
| kubectl get nodes -o wide | Nodos corriendo con detalles |
| kubectl apply -f *archivo.yaml* | Aplicar yaml en general |
| kubectl -n testing apply -f *archivo.yaml* | Aplicar en namespace específico *testing* |
| kubectl -n testing get svc | Ver servicios credados en el namespace *testing* |
| kubectl get ns | Ver los namespaces |
| kubectl -n testing get pods | Ver los pods |

#### Namespaces *testing*

    kind: Namespace
    apiVersion: v1
    metadata:
      name: testing

#### Services

    kind: Service
    apiVersion: v1
    metadata:
        name: wordpress
    spec:
        type: NodePort
        ports:
        - port: 80
          targetPort: 80
          nodePort: 30000
        selector:
          role: wordpress  

#### Replication controllers

    kind: ReplicationController
    apiVersion: v1
    metadata:
        name: wordpress
    spec:
        replicas: 1
        template:
            metadata:
                labels:
                    role: wordpress
            spec:
                containers:
                - name: wordpress
                  image: wordpress:php7.1-apache
                  imagePullPolicy: IfNotPresent
                  ports:
                  - containerPort: 80 
