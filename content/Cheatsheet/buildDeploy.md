---

title: "Build & Deploy"
tags: ["Hugo", "Cheatsheet", "Parcel"]

---

![Bash](/img/vue-js.svg?height=360px)

___

## Open Projects

___

| Comando                                         | Descripción                    | Proyecto      |
| ----------------------------------------------- | ------------------------------ | ------------- |
| parcel serve src/index.html --out-dir src/build | Servidor para desarrollo local | Documentation |
| parcel build src/index.html --no-source-maps        | Build para producción          | Documentation |

## Archived

___

| Comando | Descripción | Proyecto |
| ------- | ----------- | -------- |
|      hugo server   |     Servidor para desarrollo local        | Hugo     |
