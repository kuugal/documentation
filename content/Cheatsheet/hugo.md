---
title: "Hugo"
date: 2019-08-05T10:21:50-05:00
draft: false
tags: ["hugo", "CMS", "cheatsheet"]
menuTitle : "Hugo CMS"
---
![Hugo](/img/hugo-logo.svg?height=300px)
___

| Comando | Descripción |
| ------ | ----------- |
| hugo version   | Ver la versión instalada de hugo, comprueba si está instalado. |
| hugo help | Ver la lista de comandos disponibles. |
| hugo | Compila los archivos generando estáticos en static/pulic|
| hugo server | Build, serve, watch changes and redo on change | 

