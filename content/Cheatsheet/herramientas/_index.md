---
title: "Utilidades"
date: 2019-08-05T10:21:50-05:00
draft: false
tags: ["utils", "cheatsheet"]
menuTitle : "Herramientas"
---
![Pendientes](/img/nashville.svg?height=360px)
### Herramientas generales
___

| Herramienta | URL | Descripción |
| ------ | ------ | ----------- |
| Gatsby   | https://www.gatsbyjs.org/ | Datasources from CMS, Markdown and *data* (APIs, Databases, YAML, JSON, CSV) |
| DuckDNS | https://www.duckdns.org/ | Resuelve una dirección para ip's de servicios que cambian |
| ifconfig | https://ifconfig.me/ | Curl útiles |
| cheat.sh | http://cheat.sh/ | Curl de comandos linux |
| draw.io | https://www.draw.io/ | ```sudo snap install drawio``` |
| Information is beautiful | https://informationisbeautiful.net/ | Infografía y Visual-Data |
| Asmr | http://asmrion.com/ | asmr de gato, lluvia, olas |
| Strapi | https://strapi.io/documentation/ | Strapi is the open-source Headless CMS developers love. |
| UnDraw | https://undraw.co/illustrations | SVG illustrations |
| LogoSVG | https://worldvectorlogo.com/es | SVG logos de marcas |
| Zero Width Shortener | http://zws.im | Shorten using invisible spaces |
| rethinkdb, redis, sqlite | DB simple |
| MongoDB | is a document database, which means it stores data in JSON-like documents |
| Gridsome | https://gridsome.org/ | Gridsome makes it easy for developers to build static generated websites & apps that are fast by default 🚀 |
| Heroku | https://dashboard.heroku.com/apps | Build data-driven apps with fully managed data services |
| Surge | https://surge.sh/ | Simple, single-command web publishing. Publish HTML, CSS, and JS for free |
| Vercel | https://vercel.com/ | Free host ❔ |
| ParcelJS | https://parceljs.org/ | Blazing fast, zero configuration web application bundler |
| Photopea | https://www.photopea.com/ | Free online photoshop |
| manypixels | https://www.manypixels.co/gallery/ | Svg illustrations gallery |
| Web developer Roadmap | https://github.com/kamranahmedse/developer-roadmap | Introduction, Front-Back(end), Devops |
| goormide | https://ide.goorm.io/dashboard | Cloud IDE |
| Coolors | http://coolors.co/ | Color schemas, gradient, all color stuffs |

### Extensiones Visual Studio Code
___

| Nombre | Descripción |
| ------ | ----------- |
| Clipboard Manager | Keep a history of your copied and cut items and re-paste |
| Live server | Launch a development local Server with live reload feature for static & dynamic pages |
| Live Sass Compiler | compile/transpile your SASS/SCSS files to CSS files at realtime with live browser reload |