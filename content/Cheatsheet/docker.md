---
title: "Docker"
tags: ["Rozvo", "Docker", "Cheatsheet"]
menuTitle : "Docker"
---
![Docker](/img/docker.svg?height=360px)
### Comandos
___

| Comando | Descripción |
| ------ | ----------- |
| docker version | Versión instalada y corroba si está prendido el daemon |
| docker ps | Contenedores corriendo |
| docker ps -a | Muestra todos los contenedores incluso los muertos |
| docker image ls | Imagenes descargadas o creadas |
| docker pull *alpine* | Descarga la imagen |
| docker run *alpine* | Corre un contenedor y lo descarga si no está en local |
| docker run -it *alpine:3.11.6* sh  | Corre un contenedor con Interactive y Terminal con sh como comando de la imagen |
| docker exec -it *4cds3* | Ejecuta un contenedor usando su ID |
| docker commit *4cds3* | Crea imagen |
| docker image tag *4cds3* *nombreImagen:1.0* | Nombrar imagen creada |
| docker stop *4cds3* | Detiene un contenedor usando su ID |
| docker kill *4cds3* | Elimina un contenedor usando su ID |
| docker build -t *nombreImagen:1.0* . | Donde tengas tu Dockerfile lo ejecuta |
| docker image history *4cds3* | Historial de comandos que realiza una imagen |
| docker run -d *nginx:1.17.10* | Deja corriendo un contenedor en background Daemon  |
| Ejemplo Local:Contenedor | docker run -v ~/docker/index.html:/usr/share/nginx/html/index.html:ro -p 8080:80 -d nginx:1.17.10 |

#### Dockerfile

    FROM ubuntu
    RUN apt-get update && apt-get install figlet -y
    RUN touch /tmp/hola

