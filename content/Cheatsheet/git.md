---
title: "Git"
tags: ["Git", "Cheatsheet"]
menuTitle : "Git"
---
![Git](/img/git.svg?height=360px)

### Basics
___


| Comando | Descripción |
| ------ | ----------- |
| git init *directorio* | Create empty Git repo in specified directory. Run with no arguments to initialize the current directory as a git repository |
| git clone *repositorio.git* | Clone repo located at *repositorio.git* onto local machine. Original repo can be located on the local filesystem or on a remote machine via HTTP or SSH |
| git config user.name *nombre* | Define author name to be used for all commits in current repo. Devs commonly use --global flag to set config options for current user |
| git add *directorio* | Stage all changes in *directorio* for the next commit. Replace *directorio* with a *archivo* to change a specific file |
| git commit -m "*mensaje*" | Commit the staged snapshot, but instead of launching a text editor, use *mensaje* as the commit message |
| git merge *branch* | Merge *branch* into the current branch |
| git remote add *nombre* *url* | Create a new connection to a remote repo. After adding a remote, you can use *nombre* as a shortcut for *url* in other commands |
| git fetch *remote* *branch* | Fetches a specific *branch*, from the repo. Leave off *branch* to fetch all remote refs |
| git pull *remote* | Fetch the specified remote’s copy of current branch and immediately merge it into the local copy |
| git push *remote* *branch* | Push the branch to *remote*, along with necessary commits and objects. Creates named branch in the remote repo if it doesn’t exist |
| git branch -d *branch* | Delete branch |

{{% notice tip %}}

[Comandos extras](/bin/Atlassian-Git-Cheatsheet.pdf)

{{% /notice %}}