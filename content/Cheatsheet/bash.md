---
title: "Bash"
tags: ["Bash", "Cheatsheet", "Comandos", "Linux"]
menuTitle : "Bash"
---
![Bash](/img/linux.svg?height=360px)

Los comandos Bash son conjunto de parámetros utilizados para la administración y configuración del sistema, así como un conjunto de combinaciones especiales de teclas para realizar tareas específicas en entornos Linux/Unix mediante un intérprete de comandos Bash. 

Bash (Bourne-again shell) es un programa informático, cuya función consiste en interpretar órdenes, y un lenguaje de consola. Es una shell de Unix compatible con POSIX y el intérprete de comandos por defecto en la mayoría de las distribuciones GNU/Linux, además de macOS. También se ha llevado a otros sistemas como Windows y Android.

Su nombre es un acrónimo de Bourne-again shell ("shell Bourne otra vez") –haciendo un juego de palabras (born-again significa "nacido de nuevo") sobre la Bourne shell (sh), que fue uno de los primeros intérpretes importantes de Unix. 

### Comandos de ayuda
___

| Comando | Descripción |
| ------ | ----------- |
| man | Muestra el manual del comando que le indiquemos |
| info | Provee información del comando indicado  |
| help | Da una ayuda de los comandos |
| whatis | Da una breve descripción de los comandos  |
| type | Verificación de ubicacion y nombre único  |

### Comandos para archivos y directorios
___

| Comando | Descripción |
| ------ | ----------- |
| ls | Lista los archivos y directorios |
| sort | Ordena alfabéticamente una lista de archivos |
| cd | Cambia de directorio |
| pwd | Muestra la ruta al directorio actual |
| tree | Muestra la estructura de directorios y archivos en forma gráfica |
| mkdir | Crea un directorio |
| rmdir | Borra directorios (los directorios deben estar vacíos) |
| rm -r | Borra directorios (los directorios pueden no estar vacíos) |
| cp | Copia archivos |
| rm | Borra archivos |
| mv | Mueve o renombra archivos y directorios |
| cat | Muestra el contenido de uno o varios archivos |
| more | Ve el contenido de los archivos página a página |
| less | Ve el contenido de los archivos **<->** |
| split | Dividir archivos |
| find | Busca archivos |
| locate | Localiza archivos según una lista generada |
| updatedb | Actualiza la lista de los archivos existentes **<->** |
| whereis | Muestra la ubicación de un archivo |
| file | Muestra el tipo de archivo |
| whatis | Muestra descripción del archivo |
| wc | Muestra el total de líneas, palabras o caracteres en un archivo |
| grep | Busca un texto en un archivo |
| head | Muestra el inicio de un archivo |
| tail | Muestra el final de un archivo |
| tailf | Muestra el final de un archivo y lo que se añada en el instante (logs) |
| tr | Reemplaza caracteres en un fichero de texto |
| sed | Cambia una cadena de caracteres por otra |
| join | Cruza la información de dos archivos y muestra las partes que se repiten |
| paste | Toma la primera línea de cada archivo y las combina para formar una línea de salida |
| uniq | Elimina líneas repetidas adyacentes del archivo entrada cuando copia al archivo salida |
| cut | Sirve para seleccionar columnas de una tabla o campos de cada línea de archivo |
| ln | Crea enlaces a archivos o carpetas |
| diff | Muestra las diferencias entre dos archivos |
| fuser | Muestra que usuario tiene en uso o bloqueado un archivo o recurso |
| tar | Empaqueta archivos |
| gzip | Comprime archivos en formato .gz |
| gunzip | Descomprime archivos en formato .gz |
| compress | Comprime archivos Z |
| uncompress | Descomprime archivos Z |
| chmod | Cambia permisos de archivos y directorios |
| chown | Cambia de propietario/usuario |
| mv | Cambia de grupo |
| vi | Abre el editor de texto vi |
| nano | Abre el editor de texto nano |
| pico | Edita un fichero de texto |


### Comandos para la gestión de usuarios
___

| Comando | Descripción |
| ------ | ----------- |
| adduser | Agrega un nuevo usuario |
| useradd | Agrega un nuevo usuario |
| userdel | Borra un usuario |
| passwd | Permite cambiar la contraseña |
| su |  Cambia de usuario |
| whoami | Muestra el nombre de usuario actual |
| logname | Muestra el nombre de usuario |
| id | Muestra datos de identificación del usuario |
| finger | Da información de usuario |
| chfn | Cambia la información propocionada por el comando finger |
| who |  Muestra los usuarios actuales del sistema |
| w | Muestra detalles de los usuarios actuales aplicado al comando who |
| last | Información de los últimos usuarios que han usado el sistema |
| mail | Abre la aplicación de correo electrónico |
| pine | Lector de correo en modo texto |
| write | Manda un mensaje a la pantalla de un usuario |
| mesg |  Activa o desactiva la función de recepción de mensajes |
| wall | Envía mensaje a todos los usuarios |
| talk | Establecer una conversación/diálogo con otro usuario |
| banner | Saca un diálogo/letrero en la pantalla |
| set | Proporciona información sobre el entorno del usuario |
| addgroup | Agrega un nuevo grupo |
| groupadd | Agrega un nuevo grupo |
| chown | Cambia el propietario de un fichero |


### Comandos para la gestión de procesos
___

| Comando | Descripción |
| ------ | ----------- |
| top | Muestra los procesos que se están ejecutando y permite matarlos |
| ps | Muestra la lista de procesos del usuario |
| ps aux | Muestra la lista de procesos de la máquina |
| kill | Envía un evento concreto a un proceso |
| killall | Mata un proceso por su nombre |
| time | Mide el tiempo que tarda un proceso en ejecutarse |
| fg | Trae a primer plano un proceso parado o en segundo plano |
| bg | Pone un proceso en segundo plano |
| & | Colocado al final de la línea de un comando, lo ejecuta en segundo plano |
| nice | Ajusta la prioridad de un proceso de -20 a 19 |


### Comandos para la gestión de discos y dispositivos
___

| Comando | Descripción |
| ------ | ----------- |
| mount | Monta un disco/dispositivo |
| unmount | Desmonta un disco/dispositivo |
| df | Muestra el espacio libre de los discos/dispositivos |
| du | Muestra el espacio usado por el disco/dispositivo o un directorio |
| mkfs | Formatea un disco/dispositivo |
| fsck | Estado del disco/dispositivo |
| fdisk | Abre la aplicación para la gestión de particiones |

### Comandos para el acceso remoto
___

| Comando | Descripción |
| ------ | ----------- |
| rlogin | Se conecta a otra máquina de forma remota (remote login) |
| rsh | Se conecta a otra máquina de forma remota (remote shell) |
| ftp | Se conecta a otra máquina por el protocolo FTP |


### Comandos para apagado y reinicio del sistema
___

| Comando | Descripción |
| ------ | ----------- |
| reboot | Reinicia la máquina |
| halt | Apaga el sistema |
| shutdown | Apaga el sistema |
| init 0 | Apaga la máquina |
| init 6 | Reinicia la máquina |

### Comandos para la gestión del sistema
___

| Comando | Descripción |
| ------ | ----------- |
| uptime | Reinicia la máquina |
| exit | Apaga el sistema |
| logout | Apaga el sistema |
| nohup | Apaga la máquina |
| dmesg | Reinicia la máquina |


### Comandos para scripting
___

| Comando | Descripción |
| ------ | ----------- |
| # ?/bin/bash | Reconocimiento de sintaxis bash script |
| # Comentario | Comentario después del numeral (sharp) |
| for |  |
| VARIABLE=valor | Variable de entorno a nivel S.O (para cualquier usuario), con sudo /etc/profiles |
| export VARIABLE | Variable /> |


### Comandos de herramientas extras

| Comando | Descripción | Instalación |
| ------ | ----------- | ----------- |
| figlet *texto* | Muestra un string de forma más gráfica | `sudo apt-get install figlet` |
| ps fax |  Ver procesos que estan corriendo  | `sudo apt-get install procps` |
