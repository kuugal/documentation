---
title: "Dart"
tags: ["Dart", "Language", "Cheatsheet"]
menuTitle : "Dart"
---
![Bash](/img/dart.svg?height=360px)

### Variables
___

    //No es fuertemente tipado, indiferente a ' o "
    var nombre = 'Daniela';
    //Tipo de dato cambiará
    dynamic nombre='Daniela';
    //Podemos especificar el tipo de dato
    String nombre='Daniela';

### Constantes
___

    //final o const
    final nombre = 'Daniela';
    final String nickname='Daniela';

### Tipos de datos
___


| Tipo | Ejemplo |
| ------ | ----------- |
| int | 10 |
| double | 20.67 |
| String | "Daniela" |
| bool | true |
| var | "Por defecto es String" |
| var | [11,12,13] |
| var | {// Key: Value,} |

    Con ''' en los String se realiza un texto multilínea '''

### Funciones
___

    void myFunction(param1, param2){...}
    //Notación cascada en un main
    void main(){
        querySelector('#sample_text_id')
        ..text='Click me!'
        ..onClick.listen(reverseText);
    }

### Control de flujo
___

    if and else
    for loops
    while and do-while loops
    break and continue
    switch and case
    assert

### Excepciones
___

    //Throw
    throw FormatException('Expected at least ... ');
    //Catch
    try {

    } on OutOfLlamasException {

    } on Exception catch (e) {

    } catch (e) {

    }

### Clases y constructores
___

    class Point{
        numx, y;
        Point(this.x,this.y);
    }

#### Herencia y sobreescritura
___

    extends
    super.turnOn();
    @override
    void turnOn(){...}

### ENUM
___

    enum Color {red, green, blue}
