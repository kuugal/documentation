---
title: "Ngrok"
tags: ["Ngrok", "localhost", "Port forwarding"]
menuTitle : "Ngrok"
---
![Ngrok](/img/ngrok.png?height=300px)

### ¿Qué es ngrok?
___
Expose a local web server to the internet and therefore make it accessible to mobile devices, ngrok allows you to expose a web server running on your local machine to the internet. Just tell ngrok what port your web server is listening on.

If you don't know what port your web server is listening on, it's probably port 80, the default for HTTP. 

___
## Docker

Obtener la imagen de ngrok [recomendada](https://hub.docker.com/r/wernight/ngrok/)

    docker pull wernight/ngrok



## Binario


* Download [ngrok](https://ngrok.com/download) 

___

#### Installing your Authtoken


Many advanced features of the ngrok.com service described in further sections require that you sign up for an account. Once you've signed up, you need to configure ngrok with the authtoken that appears on your dashboard. This will grant you access to account-only features. ngrok has a simple 'authtoken' command to make this easy. Under the hood, all the authtoken command does is to add (or modify) the authtoken property in your ngrok configuration file. 

    ngrok authtoken <YOUR_AUTHTOKEN>

##### Ejemplo

    ./ngrok authtoken 1SysqGpSavm032mOsAdWuBeL8R2_55dvZNL6vGNeE2Ho3yM8e

___
## Set up service

#### tcp


    nohup ./ngrok tcp 22 & 

#### http

    nohup ./ngrok http 80 &

___

### Inspecting your traffic

ngrok provides a real-time web UI where you can introspect all of the HTTP traffic running over your tunnels. After you've started ngrok, just open http://localhost:4040 in a web browser to inspect request details.

Try making a request to your public URL. After you have, look back at the inspection UI. You will see all of the details of the request and response including the time, duration, headers, query parameters and request payload as well as the raw bytes on the wire. 


{{% notice tip %}}
Documentación oficial de [ngrok](https://ngrok.com/docs)

Documentación de la imagen de [ngrok](https://hub.docker.com/r/wernight/ngrok/) en docker
{{% /notice %}}
