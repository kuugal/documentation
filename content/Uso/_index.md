+++
title = "Uso y configuración"

weight = 6
chapter = true
pre = "<b>🌀 </b>"
+++

### Sección 6

# Uso y configuración

Cómo configurar y realizar procesos recurrentes con las herramientas.