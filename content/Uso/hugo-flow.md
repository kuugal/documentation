
---
title: "Hugo"
tags: ["hugo", "CMS"]
menuTitle : "Hugo flow"
---
### Nuevo documento
___
1. Crea un archivo con extensión markdown **.md** en el directorio pertinente

2. Agrega el header _YAML o TOML_ al archivo
```
---
title: "Hugo"
tags: ["hugo", "CMS"]
menuTitle : "Hugo flow"
---
```

2. Se agrega el logo (720px)
```
<img src="/img/hugo-logo.svg" width="720">
```
3. Agrega contenido de manera organizada
```
### Nuevo documento
___
1. Crea un archivo con extensión markdown **.md** en el directorio pertinente
```

### Deploy
___

1.
```
git add .
```
2.
```
git commit -m 'message'
```
3.
```
git push origin master
```



{{% notice note %}}
Los cambios en el servidor tardarán un minuto en verse reflejados, mientra se corre el autobuild
{{% /notice %}}

### Configuración
___

#### Rutas

* **Favicon**
/themes/learn/layouts/partials/favicon.html
* **Logo**
/themes/learn/layouts/partials/logo.html
* **Imagenes**
/themes/learn/static/images/ _(.png or .ico)_
* **Variante del tema**
/config.toml
```
[params]
  # Change default color scheme with a variant one. Can be "red", "blue", "green".
  themeVariant = "red"

```

