
---
title: "Teoría de colores"
tags: ["project", "software"]
menuTitle : "Color"
---
![Swarm?](/img/google-webdev-logo.svg?height=300px)
___

### Negro

Prestigio, poder, autoridad, miedo, incertidumbre.

### Blanco

Positivismo, pureza, seguridad.

### Rojo

Pasión, felicidad, violencia, enojo.

### Azul

Confianza, inteligencia, estabilidad, calma.

### Amarillo

Felicidad, vividez, precaución, enfermedad.

### Verde

Naturaleza, energía, riqueza, codicia.

### Naranja

Alegría, felicidad, positivismo, precaución.

### Rosa

Agradable, amistoso, accesible, infancia, romance.

### Café

Naturaleza, orgánico, honestidad, conexiones, calma.

### Morado

Prestigio, poder, lujo.
