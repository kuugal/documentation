
---
title: "Nuevo proyecto de software"
tags: ["project", "software"]
menuTitle : "Proyecto de sw"
---
![Swarm?](/img/google-webdev-logo.svg?height=300px)
___

### Trigger

1. Enfentar un problema e identificar que se puede implementar una mejor solución

    * También se puede recibir o ser consultado para desarrollar la solución directamente

    * Idea o proyecto personal


### Análisis


2. Lista de historias de usuario (Stakeholder/Usuario/Jefe de proyecto) y otros requerimientos *[Lista]* *{https://www.atlassian.com/es/agile/project-management/user-stories}*

##### historia de usuario

Es una representación de un requisito escrito en una o dos frases utilizando el lenguaje común del usuario.

* Estimables en tiempo y alcance

* Pequeñas

* Verificables: Las historias de usuario cubren requerimientos funcionales, por lo que generalmente son verificables. Cuando sea posible, la verificación debe automatizarse, de manera que pueda ser verificada en cada entrega del proyecto.
  

    | Historia | ¿Quién? | ¿Por qué? |
    | ------ | ----------- | ----- |
    | Pseudorequisito proveniente de no técnico | ¿A quién beneficia? | ¿Qué beneficio? |

3. Eliminar *inviables y no realistas*, refinar lista a requerimentos *funcionales, no funcionales* y asignarles prioridad usando criterios de exito con atributos de **calidad**, título del proyecto (Análista) *[Lista]* *{Kanban - Trello}*  
   
    - Desplegabilidad (facilidad de despliegue)
    - Simplicidad
    - Distribución del desarrollo (¿El diseño del sistema permite que equipos separados a nivel global puedan trabajar en él?)
    - Facilidad de instalación (installability)
    - Correctitud
    - Consistencia
    - Completitud
    - Robustez
    - Viabilidad
    - Disponibilidad
    - Escalabilidad
    - Interoperabilidad
    - Modificabilidad
    - Rendimiento
    - Seguridad
    - Testeabilidad (facilidad de probar el sistema)
    - Usabilidad


### Diseño

4. Definir opciones de arquitectura y alternativas que cumplan la mayoría de requerimientos; Ventajas, Desventajas; Costos; Fechas; Riesgos; Restricciones; Entornos de despliegue (Arquitecto de SW) *[Tablas]*
   *{Actualizar Kanban}*

   - Tipo de desarrollo
     - Página web
     - Pwa
     - Web app
     - Escritorio
     - App nativa
     - App multidispositivo (Flutter, Xamarin)
     - Medio no interactivo (Infografía, Diagrama de flujo)
   - 
   


5. Esquema de colores, Logotipo, Diseño de pantallas, Ilustraciones (Diseñador con Stakeholder) *[SVG, PNG]*    


### Desarrollo

6. Programarlo
