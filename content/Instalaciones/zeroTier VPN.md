---
title: "Zerotier One VPN"
---

1. Instala el cliente zerotier vpn

    #### Linux
    1. Instala

        ```
        curl -s https://install.zerotier.com | sudo bash
        ```
    2. Conecta a la red

        ```
        sudo zerotier-cli join 1d71939404c20457
        ```

    #### Windows
    
    1. Descarga el instalador desde https://www.zerotier.com/download/
    2. Click derecho sobre el icono ubicada en el area de actividades en segundo plano, Join network con el id de la red: 1d71939404c20457


1. Autorizar el dispositivo desde el panel de control de https://my.zerotier.com/

1. Accede a la ip de las maquinas que desees entrar, las direcciones son de tipo 172.22.xx.xxx

    * La dirección de la maquina en el server es: 172.22.165.209
