---
title: "Docker"
tags: ["Rozvo", "Docker"]
weight: 4
menuTitle : "Docker"
---
![Docker](/img/docker.svg?height=300px)

### ¿Qué es docker?

Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos. Docker utiliza características de aislamiento de recursos del kernel Linux, tales como cgroups y espacios de nombres (namespaces) para permitir que "contenedores" independientes se ejecuten dentro de una sola instancia de Linux, evitando la sobrecarga de iniciar y mantener máquinas virtuales.

> Es una herramienta que puede empaquetar una aplicación y sus dependencias en un contenedor virtual que se puede ejecutar en cualquier servidor Linux. Esto ayuda a permitir la flexibilidad y portabilidad en donde la aplicación se puede ejecutar, ya sea en las instalaciones físicas, la nube pública, nube privada, etc.

___
### Instalación

Docker está disponible en tres niveles:

    Docker Engine - Community
    Docker Engine - Enterprise
    Docker Enterprise

Docker Engine - Community es ideal para desarrolladores individuales y pequeños equipos que buscan comenzar con Docker y experimentar con aplicaciones basadas en contenedores.

Docker Engine - Enterprise está diseñado para el desarrollo empresarial de un contenedor en producción con seguridad de grado SLA empresarial en mente.

Docker Enterprise está diseñado para desarrollo empresarial y  equipos de TI que crean, envían y ejecutan aplicaciones empresariales críticas en producción a escala.

#### Elige SO

{{% children  %}}

{{% notice tip %}}

Documentación oficial de [Docker](https://docs.docker.com/)

{{% /notice %}}