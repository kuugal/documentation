---
title: "CentOS"
tags: ["Rozvo", "Docker"]
menuTitle : "CentOS"
pre: "<i class='fab fa-centos'></i> "
---
## OS requirements

To install Docker Engine - Community, you need a maintained version of CentOS 7. Archived versions aren’t supported or tested.

The centos-extras repository must be enabled. This repository is enabled by default, but if you have disabled it, you need to re-enable it.

The overlay2 storage driver is recommended.

___
## Install using the repository

### Set up the repository

Install required packages. yum-utils provides the yum-config-manager utility, and device-mapper-persistent-data and lvm2 are required by the devicemapper storage driver.

    sudo yum install -y yum-utils \
    device-mapper-persistent-data \
    lvm2

Use the following command to set up the stable repository.

    sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

### Install Docker Engine - Community

Install the latest version of Docker Engine - Community and containerd, or go to the next step to install a specific version:

    sudo yum install docker-ce docker-ce-cli containerd.io

If prompted to accept the GPG key, verify that the fingerprint matches 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35, and if so, accept it.

Docker is installed but not started. The docker group is created, but no users are added to the group.

To install a specific version of Docker Engine - Community, list the available versions in the repo, then select and install:

a. List and sort the versions available in your repo. This example sorts results by version number, highest to lowest, and is truncated:

    yum list docker-ce --showduplicates | sort -r

    docker-ce.x86_64  3:18.09.1-3.el7                     docker-ce-stable
    docker-ce.x86_64  3:18.09.0-3.el7                     docker-ce-stable
    docker-ce.x86_64  18.06.1.ce-3.el7                    docker-ce-stable
    docker-ce.x86_64  18.06.0.ce-3.el7                    docker-ce-stable

The list returned depends on which repositories are enabled, and is specific to your version of CentOS (indicated by the .el7 suffix in this example).

b. Install a specific version by its fully qualified package name, which is the package name (docker-ce) plus the version string (2nd column) starting at the first colon (:), up to the first hyphen, separated by a hyphen (-). For example, docker-ce-18.09.1.

    sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io

Docker is installed but not started. The docker group is created, but no users are added to the group.

Start Docker.

    sudo systemctl start docker

Verify that Docker Engine - Community is installed correctly by running the hello-world image.

    sudo docker run hello-world

This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

Docker Engine - Community is installed and running. You need to use sudo to run Docker commands. Continue to Linux postinstall to allow non-privileged users to run Docker commands and for other optional configuration steps.

## Install from a package

If you cannot use Docker’s repository to install Docker, you can download the .rpm file for your release and install it manually. You need to download a new file each time you want to upgrade Docker Engine - Community.

Go to https://download.docker.com/linux/centos/7/x86_64/stable/Packages/ and download the .rpm file for the Docker version you want to install.

Install Docker Engine - Community, changing the path below to the path where you downloaded the Docker package.

    sudo yum install /path/to/package.rpm

Docker is installed but not started. The docker group is created, but no users are added to the group.

Start Docker.

    sudo systemctl start docker

Verify that Docker Engine - Community is installed correctly by running the hello-world image.

    sudo docker run hello-world

This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

Docker Engine - Community is installed and running. You need to use sudo to run Docker commands. Continue to Post-installation steps for Linux to allow non-privileged users to run Docker commands and for other optional configuration steps.

___
## Uninstall Docker Engine - Community

Uninstall the Docker package:

    sudo yum remove docker-ce

Images, containers, volumes, or customized configuration files on your host are not automatically removed. To delete all images, containers, and volumes:

    sudo rm -rf /var/lib/docker

You must delete any edited configuration files manually.
