---
title: "Windows 10 pro"
tags: ["Rozvo", "Docker"]
menuTitle :  " Windows"
pre: "<i class='fab fa-windows'></i> "
---
## Requisitos SO
___

Windows 10 64-bit: Pro, Enterprise, or Education (Build 15063 or later).
Hyper-V and Containers Windows features must be enabled.

The following hardware prerequisites are required to successfully run Client Hyper-V on Windows 10:

* 64 bit processor with Second Level Address Translation (SLAT)
* 4GB system RAM
* BIOS-level hardware virtualization support must be enabled in the BIOS settings. For more information, see Virtualization.

___

### Descarga [Docker desktop](https://hub.docker.com/?overlay=onboarding)

___
##### Docker toolbox

{{% notice warning %}}

Puedes instalar docker toolbox para windows que no cumplen los requerimientos https://docs.docker.com/toolbox/toolbox_install_windows/

{{% /notice %}}

