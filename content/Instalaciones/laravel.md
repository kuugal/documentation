---
title: "Laravel 6"
draft: false
weight: 2
tags: ["laravel", "framework", "php"]
menuTitle : "Laravel"
---
![Laravel](/img/laravel.svg?height=300px)


### Server Requirements

The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

* PHP >= 7.2.0
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

___

## Installing Laravel

Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine.
Via Laravel Installer

First, download the Laravel installer using [Composer](https://getcomposer.org/):

    composer global require laravel/installer

___


| Comando | Descripción |
| ------ | ----------- |
| laravel new proyecto   | Crear nuevo proyecto laravel. |



{{% notice tip %}}

Documentación oficial de [Laravel](https://laravel.com/docs/)

{{% /notice %}}